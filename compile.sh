#!/bin/bash
# vim: set ts=2:sw=2:sts=2:expandtab:


SCRIPT_ROOT="$(dirname $(realpath $0))"

SOURCE_DIR="${SCRIPT_ROOT}"
BINARY_DIR="${SCRIPT_ROOT}/build"

cd "${SOURCE_DIR}"

if ! [ -d "${BINARY_DIR}" ]
then
	mkdir --verbose "${BINARY_DIR}"
else
	rm --recursive --force --verbose  "${BINARY_DIR}"
	mkdir --verbose "${BINARY_DIR}"
fi

cd "${BINARY_DIR}"

APP_NAME="a"
SOURCE="${SOURCE_DIR}/${APP_NAME}.cpp"
CC=""
CXX=""
PLAT=""
PLAT_C=""
PLAT_N=""
ARCH=""
ARCH_N=""
EXT=""
PFX=""


# Red Hat Enterprise Linux Workstation/Server
PLAT="Red Hat Enterprise Linux"
PLAT_C="el"
{
	PLAT_N=6
	CC="gcc44"
	CXX="g++44"
	echo "Building for ${PLAT} ${PLAT_N}..."
	for ARCH_N in 32 64
	do
		${CXX} -m${ARCH_N} ${SOURCE} -o ${APP_NAME}-${PLAT_C}${PLAT_N}-${ARCH_N}${EXT}
	done
}
{
	PLAT_N=7
	CC="gcc"
	CXX="g++"
	echo "Building for ${PLAT} ${PLAT_N}..."
	for ARCH_N in 32 64
	do
		${CXX} -m${ARCH_N} ${SOURCE} -o ${APP_NAME}-${PLAT_C}${PLAT_N}-${ARCH_N}
	done
}


# Developer Toolsets
# 64-bit: devtoolset-{4,6,7,8}
# 32-bit: devtoolset-{4,6,7,8}-libstdc++-devel.i686
PLAT="devtoolset-"
PLAT_C="dts"
for PLAT_N in 4 6 7 8
do
	CC="gcc"
	CXX="g++"
	echo "Building with ${PLAT}${PLAT_N}..."
	for ARCH_N in 32 64
	do
		scl enable ${PLAT}${PLAT_N} "${CXX} -m${ARCH_N} ${SOURCE} -o ${APP_NAME}-${PLAT_C}${PLAT_N}-${ARCH_N}"
	done
done


# Windows (cross-compiled)
# C   32-bit: mingw32-gcc
# C++ 32-bit: mingw32-gcc-c++
# C   64-bit: mingw64-gcc
# C++ 64-bit: mingw64-gcc-c++
PLAT="Windows (mingw32)"
PLAT_C="win"
echo "Building for ${PLAT}..."

ARCH=i686
ARCH_N=32
CC="${ARCH}-w64-mingw32-gcc"
CXX="${ARCH}-w64-mingw32-g++"
${CXX} -m${ARCH_N} -mconsole ${SOURCE} -o ${APP_NAME}-${PLAT_C}-${ARCH_N}.exe

ARCH=x86_64
ARCH_N=64
CC="${ARCH}-w64-mingw32-gcc"
CXX="${ARCH}-w64-mingw32-g++"
${CXX} -m${ARCH_N} -mconsole ${SOURCE} -o ${APP_NAME}-${PLAT_C}-${ARCH_N}.exe



